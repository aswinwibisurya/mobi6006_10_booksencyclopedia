package com.example.aswin.a10_booksencyclopedia;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    RecyclerView rvBooks;
    BooksAdapter adapter;
    RequestQueue queue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rvBooks = findViewById(R.id.rvBooks);
        adapter = new BooksAdapter(this);

        rvBooks.setLayoutManager(new LinearLayoutManager(this));
        rvBooks.setAdapter(adapter);

        queue = Volley.newRequestQueue(this);
        queue.add(getBooks("computer"));
    }


    JsonObjectRequest getBooks(String term) {
        String URL = "https://itunes.apple.com/search?media=ebook&term=" + term;

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ArrayList<Book> listBook = new ArrayList<>();
                try {
                    JSONArray arrResults = response.getJSONArray("results");

                    for(int i=0; i< arrResults.length(); i++) {
                        JSONObject objResult = arrResults.getJSONObject(i);
                        Book book = new Book();
                        book.title = objResult.getString("trackName");
                        book.author = objResult.getString("artistName");
                        book.price = objResult.getDouble("price");
                        book.thumbnailURL = objResult.getString("artworkUrl60");

                        listBook.add(book);
                    }

                    adapter.setListBooks(listBook);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        return req;
    }
}
