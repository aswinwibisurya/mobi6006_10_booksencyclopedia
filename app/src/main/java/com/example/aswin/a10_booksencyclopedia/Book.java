package com.example.aswin.a10_booksencyclopedia;

public class Book {
    public String title;
    public String author;
    public double price;
    public String thumbnailURL;
}
